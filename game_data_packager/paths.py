#!/usr/bin/python3
# encoding=utf-8

import os

if os.environ.get('GDP_UNINSTALLED'):
    CONFIG = './etc/game-data-packager.conf'

    if 'GDP_PKGDATADIR' in os.environ:
        DATADIR = os.environ['GDP_PKGDATADIR']
    else:
        DATADIR = './out'

    ETCDIR = './etc'
else:
    CONFIG = '/etc/game-data-packager.conf'

    if 'GDP_PKGDATADIR' in os.environ:
        DATADIR = os.environ['GDP_PKGDATADIR']
    elif os.path.isdir('/usr/share/games/game-data-packager'):
        DATADIR = '/usr/share/games/game-data-packager'
    else:
        DATADIR = '/usr/share/game-data-packager'

    ETCDIR = '/etc/game-data-packager'
